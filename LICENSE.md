# Author: Zachary Wartell
# Git: https://gitlab.com/zwartell/Unity_XR_Tutorial_1
# License

- Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License 
- http://creativecommons.org/licenses/by-nc-sa/4.0/ 